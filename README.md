# bencode-tools

bencode-tools is a collection of tools for manipulating bencoded data.
See http://en.wikipedia.org/wiki/Bencode for information on bencode
format.

This was formely named 'typevalidator'. With the introduction of
libbencodetools the project is named 'bencode-tools'.

The project is hosted at https://gitlab.com/heikkiorsila/bencodetools

# Dependencies

* Python dependencies for usage: None

* To install dependencies for development, run: ```pip install -r requirements_dev.txt```
